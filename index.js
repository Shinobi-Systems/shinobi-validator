let alreadyLoadedOnce = null;
module.exports = (s,app,lang,config) => {
    const {
        checkForEC2,
    } = require('./libs/ec2.js')
    const {
        checkSubscription,
    } = require('./libs/subscription.js')(s,app,lang,config)
    const {
        checkIfSystemCanAddMoreCameras,
    } = require('./libs/utils.js')(s,app,lang,config)
    async function startupSystemChecks(){
        config.userHasSubscribed = await checkSubscription(config.subscriptionId)
        config.isEC2 = await checkForEC2()
        return {
            isEC2: config.isEC2,
            userHasSubscribed: config.userHasSubscribed,
        }
    }
    if(!alreadyLoadedOnce){
        console.log('Add Startup C')
        alreadyLoadedOnce = true;
        s.onProcessReady(async function(){
            await startupSystemChecks()
        })
    }
}
