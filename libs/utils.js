const fetch  = require('node-fetch');
const { AbortController } = require('node-abort-controller')
module.exports = (s,app,lang,config) => {
    const fetchTimeout = (url, ms, { signal, ...options } = {}) => {
        const controller = new AbortController();
        const promise = fetch(url, { signal: controller.signal, ...options });
        if (signal) signal.addEventListener("abort", () => controller.abort());
        const timeout = setTimeout(() => controller.abort(), ms);
        return promise.finally(() => clearTimeout(timeout));
    }
    function checkIfSystemCanAddMoreCameras(){
        const cameraCountChecks = [
            {kind: 'ec2', maxCameras: 2, conditions: `config.isEC2`},
            {kind: 'highCoreCount', maxCameras: 50, conditions: `config.isHighCoreCount`},
        ]
        let canAddCameras = true
        if(!config.userHasSubscribed){
            function passesCheck(evaluation){
                if(eval(evaluation.conditions)){
                    let monitorCountOnSystem = 0;
                    const allGroups = Object.keys(s.group)
                    allGroups.forEach((groupKey) => {
                        const monitorIds = Object.keys(s.group[groupKey].rawMonitorConfigurations)
                        monitorIds.forEach((monitorId) => {
                            // const monitorConfig = s.group[groupKey].rawMonitorConfigurations[monitorId]
                            ++monitorCountOnSystem
                        })
                    })
                    if(monitorCountOnSystem >= evaluation.maxCameras){
                        return false
                    }
                }
                return true
            }
            for (let i = 0; i < cameraCountChecks.length; i++) {
                let evaluation = cameraCountChecks[i]
                if(!passesCheck(evaluation))return false;
            }
        }
        return true
    }
    function sanitizeMonitorConfig(monitorConfig){
        const sanitized = {}
        const errors = {}
        const availableKeys = [
            {name: 'ke', type: 'string'},
            {name: 'mid', type: 'string'},
            {name: 'name', type: 'string'},
            {name: 'details', type: 'longtext'},
            {name: 'type', type: 'string', defaultTo: 'h264'},
            {name: 'ext', type: 'string', defaultTo: 'mp4'},
            {name: 'protocol', type: 'string', defaultTo: 'rtsp'},
            {name: 'host', type: 'string', missingHalt: true },
            {name: 'path', type: 'string', missingHalt: true },
            {name: 'port', type: 'integer', defaultTo: 554},
            {name: 'fps', type: 'integer', defaultTo: null},
            {name: 'mode', type: 'string', defaultTo: 'stop'},
            {name: 'width', type: 'integer', defaultTo: 640},
            {name: 'height', type: 'integer', defaultTo: 480},
        ];
        for(item of availableKeys){
            const column = item.name;
            const type = item.type;
            const monitorValue = monitorConfig[column]
            let newValue = monitorValue;
            switch(item.type){
                case'string':
                case'longtext':
                    if(monitorValue instanceof String){

                    }else{
                        newValue = `${monitorValue}`;
                        errors[column] = `corrected ${type} type : ${typeof monitorValue}`;
                    }
                break;
                case'integer':
                    if(!isNaN(monitorValue)){

                    }else{
                        newValue = parseInt(monitorValue);
                        errors[column] = `corrected ${type} type : ${typeof monitorValue}`;
                    }
                break;
            }
            sanitized[column] = newValue;
        }
        return {
            sanitized,
            errors
        }
    }
    return {
        checkIfSystemCanAddMoreCameras,
        fetchTimeout
    }
}
