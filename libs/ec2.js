const { fetchTimeout } = require('./utils.js')
async function checkForEC2(){
    try{
        await fetchTimeout('http://169.254.169.254/latest/meta-data/',600)
        console.log('Amazon EC2 Detected')
        return true
    }catch(err){
        return false
    }
}
module.exports = {
    checkForEC2,
}
