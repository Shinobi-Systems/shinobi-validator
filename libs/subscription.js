const { fetchTimeout } = require('./utils.js')
module.exports = (s,app,lang,config) => {
    const checkSubscription = (subscriptionId,oldCallback) => {
        return new Promise((resolve) => {
            function callback(response){
                config.userHasSubscribed = !!response
                resolve(response)
                if(oldCallback)oldCallback(response);
            }
            function subscriptionFailed(){
                console.error('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                console.error('This Install of Shinobi is NOT Activated')
                console.error('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                console.log('This Install of Shinobi is NOT Activated')
                console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                console.log('https://licenses.shinobi.video/subscribe')
            }
            if(subscriptionId && subscriptionId !== 'sub_XXXXXXXXXXXX' && !config.disableOnlineSubscriptionCheck){
                var url = 'https://licenses.shinobi.video/subscribe/check?subscriptionId=' + subscriptionId
                var hasSubcribed = false
                fetchTimeout(url,30000,{
                    method: 'GET',
                })
                .then(response => response.text())
                .then(function(body){
                    var json = s.parseJSON(body)
                    hasSubcribed = json && !!json.ok
                    var i;
                    for (i = 0; i < s.onSubscriptionCheckExtensions.length; i++) {
                        const extender = s.onSubscriptionCheckExtensions[i]
                        hasSubcribed = extender(hasSubcribed,json,subscriptionId)
                    }
                    callback(hasSubcribed)
                    if(hasSubcribed){
                        s.systemLog('This Install of Shinobi is Activated')
                        if(!json.expired && json.timeExpires){
                            s.systemLog(`This License expires on ${json.timeExpires}`)
                        }
                    }else{
                        subscriptionFailed()
                    }
                }).catch((err) => {
                    if(err)console.log(err)
                    subscriptionFailed()
                    callback(false)
                })
            }else{
                var i;
                for (i = 0; i < s.onSubscriptionCheckExtensions.length; i++) {
                    const extender = s.onSubscriptionCheckExtensions[i]
                    hasSubcribed = extender(false,{},subscriptionId)
                }
                if(hasSubcribed === false){
                    subscriptionFailed()
                }
                callback(hasSubcribed)
            }
        })
    }
    return {
        checkSubscription,
    }
}
